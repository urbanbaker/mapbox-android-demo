package com.mapbox.mapboxandroiddemo;

import java.util.ArrayList;
import java.util.List;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.util.Log;

public class PhotoReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("photo-marker", "received new photo! ");
        context.startService(new Intent(context, PhotoMarkerService.class));

    }

    public static class PhotoMarkerService extends IntentService {

        public PhotoMarkerService() {
            super("photo marker service");
        }

        @Override
        protected void onHandleIntent(Intent intent) {
            StringBuilder whereBuilder = new StringBuilder();
            whereBuilder.append("(" + MediaStore.Files.FileColumns.MEDIA_TYPE + " = "
                    + MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE + " OR " + MediaStore.Files.FileColumns.MEDIA_TYPE
                    + " = " + MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO + ")");

            whereBuilder.append(" AND " + MediaStore.MediaColumns.DATE_ADDED + " > "
                    + (System.currentTimeMillis() / 1000 - 10));

            List<String> args = new ArrayList<String>();

            // whereBuilder.append(" )");

            Cursor imageCur = getContentResolver().query(
                    MediaStore.Files.getContentUri("external"),
                    new String[] { Images.ImageColumns.DATA, Images.ImageColumns.LATITUDE,
                            Images.ImageColumns.LONGITUDE, Images.ImageColumns.DESCRIPTION }, whereBuilder.toString(),
                    args.toArray(new String[args.size()]), ""/*
                                                              * DBUtils.
                                                              * chronologicalSortOrder
                                                              * (true)
                                                              */);
            
            SharedPreferences prefs = this.getSharedPreferences("photo-marker", Context.MODE_PRIVATE);
            while (imageCur.moveToNext()) {
                double lat = imageCur.getDouble(1);
                double lng = imageCur.getDouble(2);
                Log.e("photo-marker", "photo id " + imageCur.getString(0) + " is at " + lat + " " + lng);
                prefs.edit().putString(imageCur.getString(0), lat + " " + lng).commit();
            }

        }

    }

}
