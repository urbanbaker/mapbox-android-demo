package com.mapbox.mapboxandroiddemo;

import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mapbox.mapboxsdk.api.ILatLng;
import com.mapbox.mapboxsdk.geometry.BoundingBox;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.overlay.GpsLocationProvider;
import com.mapbox.mapboxsdk.overlay.Icon;
import com.mapbox.mapboxsdk.overlay.Marker;
import com.mapbox.mapboxsdk.overlay.PathOverlay;
import com.mapbox.mapboxsdk.overlay.UserLocationOverlay;
import com.mapbox.mapboxsdk.tileprovider.tilesource.ITileLayer;
import com.mapbox.mapboxsdk.tileprovider.tilesource.MapboxTileLayer;
import com.mapbox.mapboxsdk.views.MapView;
import com.mapbox.mapboxsdk.views.MapViewListener;
import com.mapbox.mapboxsdk.views.util.TilesLoadedListener;

public class MainActivity extends Activity {

    private MapView mv;
    private UserLocationOverlay myLocationOverlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        // 初始化地圖
        mv = (MapView) findViewById(R.id.mapview);
        mv.setMinZoomLevel(mv.getTileProvider().getMinimumZoomLevel());
        mv.setMaxZoomLevel(mv.getTileProvider().getMaximumZoomLevel());
        mv.setCenter(mv.getTileProvider().getCenterCoordinate());
        mv.setCenter(new LatLng(25.0606629393988, 121.560341119766));
        mv.setZoom(17);

        // R.string.streetMapId裡面放的就是我們的地圖在mapbox上的id
        final EditText mapIdField = (EditText) findViewById(R.id.mapIdField);
        mapIdField.setText(getString(R.string.streetMapId));

        // Adds an icon that shows the location of the user
        showUserLocation();

        // 顯示行進歷程
        addPath();

        mv.loadFromGeoJSONURL("https://gist.githubusercontent.com/tmcw/10307131/raw/21c0a20312a2833afeee3b46028c3ed0e9756d4c/map.geojson");
        // setButtonListeners();

        // 顯示景點
        addMarkers();

        // 定義按下景點的反應
        listenToInput();

        // 如果要在地圖的tile載入前後做一些事，可以利用這個listener
        mv.setOnTilesLoadedListener(new TilesLoadedListener() {
            @Override
            public boolean onTilesLoaded() {
                return false;
            }

            @Override
            public boolean onTilesLoadStarted() {
                // TODO Auto-generated method stub
                return false;
            }
        });

        Button setMapIdButton = (Button) findViewById(R.id.setMapId);
        setMapIdButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                replaceMapView(mapIdField.getText().toString());
            }

        });

        // 用來向mapbox回報issue的按鈕，我們不會用到
        Button bugsBut = changeButtonTypeface((Button) findViewById(R.id.bugsButton));
        bugsBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://github.com/mapbox/mapbox-android-sdk/issues?state=open";
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(i);
            }
        });
    }

    private void addPath() {
        PathOverlay pathOverlay = new PathOverlay(Color.MAGENTA, 5);
        pathOverlay.addPoint(25.060858, 121.557953);
        pathOverlay.addPoint(25.060849, 121.558982);
        pathOverlay.addPoint(25.060815, 121.559682);
        pathOverlay.addPoint(25.060482, 121.560283);
        pathOverlay.addPoint(25.060897, 121.560739);
        mv.addOverlay(pathOverlay);
    }

    private void addMarkers() {
        Marker m = new Marker(mv, "放放堂", "fun fun town", new LatLng(25.060858, 121.557953));
        // 預設大頭針顯示，"art-gallery"是mapbox預設的圖像名稱之一
        m.setIcon(new Icon(this, Icon.Size.MEDIUM, "art-gallery", "FF0000"));
        mv.addMarker(m);

        m = new Marker(mv, "7-ELEVEN", null, new LatLng(25.060397, 121.565521));
        // 自行設定圖片
        // 注意!! 座標在圖片的中心，但圖片只有上半部可以按，下半部不能按
        // 所以圖片要做成下半部透明
        m.setMarker(getResources().getDrawable(R.drawable.nine_eleven));
        mv.addMarker(m);
    }

    private void showUserLocation() {
        myLocationOverlay = new UserLocationOverlay(new GpsLocationProvider(this), mv);
        myLocationOverlay.setDrawAccuracyEnabled(true);
        // 設定地圖隨使用者移動
        myLocationOverlay.enableFollowLocation();
        mv.getOverlays().add(myLocationOverlay);
    }

    private void listenToInput() {
        mv.setMapViewListener(new MapViewListener() {

            @Override
            public void onShowMarker(MapView pMapView, Marker pMarker) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onHidemarker(MapView pMapView, Marker pMarker) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onTapMarker(MapView pMapView, Marker pMarker) {
                Toast.makeText(getBaseContext(), pMarker.getTitle(), Toast.LENGTH_LONG).show();
                Uri uri = Uri.parse("http://www.google.com/#q=" + pMarker.getTitle());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }

            @Override
            public void onLongPressMarker(MapView pMapView, Marker pMarker) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onTapMap(MapView pMapView, ILatLng pPosition) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onLongPressMap(MapView pMapView, ILatLng pPosition) {
                // TODO Auto-generated method stub

            }

        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        addPhotoMarkers();
        myLocationOverlay.enableMyLocation();
    }

    @Override
    protected void onPause() {
        super.onPause();
        myLocationOverlay.disableMyLocation();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_activity_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.menuItemStreets:
            replaceMapView(getString(R.string.streetMapId));
            return true;
        case R.id.menuItemSatellite:
            replaceMapView(getString(R.string.satelliteMapId));
            return true;
        case R.id.menuItemTerrain:
            replaceMapView(getString(R.string.terrainMapId));
            return true;
        case R.id.menuItemOutdoors:
            replaceMapView(getString(R.string.outdoorsMapId));
            return true;
        case R.id.menuItemWoodcut:
            replaceMapView(getString(R.string.woodcutMapId));
            return true;
        case R.id.menuItemPencil:
            replaceMapView(getString(R.string.pencilMapId));
            return true;
        case R.id.menuItemSpaceship:
            replaceMapView(getString(R.string.spaceShipMapId));
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    private void addPhotoMarkers() {
        SharedPreferences prefs = this.getSharedPreferences("photo-marker", Context.MODE_PRIVATE);
        Map<String, String> map = (Map<String, String>) prefs.getAll();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String[] coords = entry.getValue().split(" ");
            double lat = Double.parseDouble(coords[0]);
            double lng = Double.parseDouble(coords[1]);
            if (lat > 0 && lng > 0) {
                Marker m = new Marker(mv, "photo marker", entry.getKey(), 
                        new LatLng(lat, lng));
                m.setIcon(new Icon(this, Icon.Size.LARGE, "art-gallery", "0000FF"));
                // how to efficiently load the photo?
//                m.setImage(new  BitmapDrawable(getResources(), entry.getKey()));
                mv.addMarker(m);
            }

        }
    }

    protected void replaceMapView(String layer) {

        if (TextUtils.isEmpty(layer)) {
            return;
        }

        ITileLayer source;
        BoundingBox box;

        source = new MapboxTileLayer(layer);

        mv.setTileSource(source);
        box = source.getBoundingBox();
        mv.setScrollableAreaLimit(box);
        mv.setMinZoomLevel(mv.getTileProvider().getMinimumZoomLevel());
        mv.setMaxZoomLevel(mv.getTileProvider().getMaximumZoomLevel());
        /*
         * mv.setCenter(mv.getTileProvider().getCenterCoordinate());
         * mv.setZoom(0);
         */

        EditText mapIdField = (EditText) findViewById(R.id.mapIdField);
        mapIdField.setText(layer);
    }

    private Button changeButtonTypeface(Button button) {
        return button;
    }

    // 以下在範例程式中沒有用到，可能僅供參考

    public LatLng getMapCenter() {
        return mv.getCenter();
    }

    public void setMapCenter(ILatLng center) {
        mv.setCenter(center);
    }

    /**
     * Method to show settings in alert dialog On pressing Settings button will
     * lauch Settings Options - GPS
     */
    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getBaseContext());

        // Setting Dialog Title
        alertDialog.setTitle("GPS settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                getBaseContext().startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
}
